import java.util.List;
import java.util.stream.Collectors;

public class Pack
{
    private List<Product> products;

    public Pack(List<Product> products)
    {
        this.products = products;
    }

    public double getValue()
    {
        return products.stream().mapToDouble(Product::getCost).sum();
    }
}
