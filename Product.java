public class Product
{
    private double cost;

    public Product(double cost)
    {
        this.cost = cost;
    }

    public double getCost()
    {
        return cost;
    }
}
