import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Client
{

    public static void main(String[] args)
    {
        Product product = new Product(13.02);
        Pack pack = new Pack(Arrays.asList(new Product(13.02), new Product(5.67), new Product(2.12)));

        List<Object> products = Arrays.asList(product, pack);

        Object randomProduct = products.get(new Random().nextInt(products.size()));

        double value = 0.0;

        if (randomProduct instanceof Product)
        {
            value = ((Product) randomProduct).getCost();
        }
        else if (randomProduct instanceof Pack)
        {
            value = ((Pack) randomProduct).getValue();
        }

        System.out.println(value);
    }
}
